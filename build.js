var Handlebars = require('handlebars');
var fs         = require('fs');
var models     = require('./models.json');
var apis       = {};
var args       = process.argv.slice(2);
var options    = {
  target: 'basic',
  dist: './dist/assets/templates/',
  javascript: 'es6',
  api: 'http://localhost:4000/'
};
var templates   = {
  vue: {
    create: fs.readFileSync('./templates/vue/create.vue', 'utf8', (err, data) => {return data;}),
    update: fs.readFileSync('./templates/vue/update.vue', 'utf8', (err, data) => {return data;}),
    delete: fs.readFileSync('./templates/vue/delete.vue', 'utf8', (err, data) => {return data;}),
    forms: {
      create: fs.readFileSync('./templates/vue/forms/create.vue', 'utf8', (err, data) => {return data;}),
      update: fs.readFileSync('./templates/vue/forms/update.vue', 'utf8', (err, data) => {return data;}),
      delete: fs.readFileSync('./templates/vue/forms/delete.vue', 'utf8', (err, data) => {return data;})
    }
  }
}

for (var i = 0; i < args.length; i++) {
  var arg = JSON.parse('{' + args[ i ].replace('--', '"').replace('=', '":"') + '"}');
  var arg = args[ i ].replace('--', '').split('=');
  options[ arg[ 0 ] ] = arg[ 1 ];
}

/////////////////////////////////
// FIRST LEVEL -> MODEL NAME
/////////////////////////////////
for (var modelKey in models) {

  if (models.hasOwnProperty(modelKey)) {

    var model = models[ modelKey ];
    var modelClient = {
      create: {
        template: null,
        script: null
      },
      update: {
        template: null,
        script: null
      },
      read: {
        template: null,
        script: null
      },
      delete: {
        template: null,
        script: null
      }
    };
    var content = {
      create: "",
      update: "",
      delete: ""
    }

    /////////////////////////////////
    // SECOND LEVEL -> MODEL FIELDS
    /////////////////////////////////
    for (var modelField in model) {
      if (model.hasOwnProperty(modelField)) {

        var fieldInfos = model[ modelField ];
        var required = fieldInfos.required ? "required" : "";

        if ( fieldInfos.type && fieldInfos.type === "String" ) {
          var inputType = fieldInfos.inputType ? fieldInfos.inputType : "text";
          var placeholder = fieldInfos.placeholder ? fieldInfos.placeholder : "";

          content.create += createInput( modelField, inputType, placeholder, required );
          content.update += updateInput( modelField, inputType, placeholder, required );

          if (modelField === "id" || modelField === "_id") {
            content.delete += deleteInput( modelField, inputType, placeholder, required );
          }

        }
      }
    }

    modelClient.create.template = content.create;
    modelClient.update.template = content.update;
    modelClient.delete.template = content.delete;

    apis[ modelKey ] = modelClient;

  }

}

/////////////////////////////////
// WRITE THE API FILES
/////////////////////////////////

switch (options.target) {
  case 'basic': createBasicFiles(); break;
  case 'vue': createVueFiles(); break;
}

/////////////////////////////////
// FUNCTIONS TO CREATE FILES
/////////////////////////////////

function createBasicFiles() {

}

function createVueFiles() {
  for (var modelKey in apis) {
    if (apis.hasOwnProperty(modelKey)) {


      /*
      *   CREATE FORM -> CREATE
      */

      var mainCreate = templates[ options.target ].create;
      var createTemplate = Handlebars.compile(mainCreate);
      var formCreate = createForm({
        id: modelKey,
        content: apis[ modelKey ].create.template,
        apiUrl: options.api + modelKey
      });

      fs.writeFile('./test/' + modelKey + 'Create.vue', createTemplate({
        form: formCreate
      }), function(err) {
          if(err) {
              return console.log(err)
          }

          console.log("The file was saved!");
      });

      /*
      *   CREATE FORM -> UPDATE
      */

      var mainUpdate = templates[ options.target ].update;
      var updateTemplate = Handlebars.compile(mainUpdate);
      var formUpdate = updateForm({
        id: modelKey,
        content: apis[ modelKey ].update.template,
        apiUrl: options.api + modelKey
      }).replace('<vue-escape>', '{{').replace('</vue-escape>', '}}');

      var vueTransformed = updateTemplate({
        form: formUpdate
      }).replace('<vue-escape>', '{{').replace('</vue-escape>', '}}');

      fs.writeFile('./test/' + modelKey + 'Update.vue', vueTransformed, function(err) {
          if(err) {
              return console.log(err);
          }

          console.log("The file was saved!");
      });

      /*
      *   CREATE FORM -> READ
      */

      /*
      *   CREATE FORM -> DELETE
      */
      var mainDelete = templates[ options.target ].delete;
      var deleteTemplate = Handlebars.compile(mainDelete);
      var formDelete = deleteForm({
        id: modelKey,
        content: apis[ modelKey ].delete.template,
        apiUrl: options.api + modelKey
      }).replace('<vue-escape>', '{{').replace('</vue-escape>', '}}');

      var vueTransformed = deleteTemplate({
        form: formDelete
      }).replace('<vue-escape>', '{{').replace('</vue-escape>', '}}');

      fs.writeFile('./test/' + modelKey + 'Delete.vue', vueTransformed, function(err) {
          if(err) {
              return console.log(err);
          }

          console.log("The file was saved!");
      });

    }
  }
}

/////////////////////////////////
// FUNCTIONS TO CREATE TEMPLATES
/////////////////////////////////
function createForm(data) {
  var source = templates[ options.target ].forms.create;
  var template = Handlebars.compile(source);
  return template(data);
}

function updateForm(data) {
  var source = templates[ options.target ].forms.update;
  var template = Handlebars.compile(source);
  return template(data);
}

function deleteForm(data) {
  var source = templates[ options.target ].forms.delete;
  var template = Handlebars.compile(source);
  return template(data);
}

function createInput( modelField, inputType, placeholder, required ) {
  return '\t\t<input name="' + modelField +
              '" type="' + inputType +
              '" placeholder="' + placeholder +
              '" ' + required + '>\n';
}

function updateInput( modelField, inputType, placeholder, required ) {
  return '\t\t<input name="' + modelField +
              '" type="' + inputType +
              '" placeholder="' + placeholder +
              '" value="<vue-escape> ' + modelField + ' </vue-escape>"'
              + required + '>\n';
}

function deleteInput( modelField, inputType, placeholder, required ) {
  return '\t\t<input name="' + modelField +
              '" type="' + inputType +
              '" placeholder="' + placeholder +
              '" value="<vue-escape> ' + modelField + ' </vue-escape>"'
              + required + '>\n';
}



function createScript( modelName ) {
  var script = '';
  switch (options.target) {
    case 'basic':
      script = createBasicScript();
    break;
    case 'vue':
      script = createVueScript();
    break;
  }

  var source = script;
  var template = Handlebars.compile(source);
  var data = 'var form = document.getElementById("create-' + modelName + '-form");';
  return template(data);
}

function createVueScript() {
  var script = '';
  switch (options.javascript) {
    case 'es6':
    script = 'export default {  }';
    break;
    case 'vue': script = ''; break;
  }

  return script;
}
