import Templating from './Templating';

export default class Create extends Templating {

  constructor(models, options) {

    super( models, options );

  }

  input() {
    return '\t\t<input name="' + modelField +
                '" type="' + inputType +
                '" placeholder="' + placeholder +
                + required + '>\n';
  }
  
}
