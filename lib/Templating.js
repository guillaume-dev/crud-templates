import fs from 'fs';

export default class Templating {

  constructor(models, options) {

    this.content = "";

    for (var modelKey in models) {

      if (models.hasOwnProperty(modelKey)) {

        for (var modelField in model) {
          if (model.hasOwnProperty(modelField)) {

            var fieldInfos = model[ modelField ];
            var required = fieldInfos.required ? "required" : "";

            if ( fieldInfos.type && fieldInfos.type === "String" ) {
              var inputType = fieldInfos.inputType ? fieldInfos.inputType : "text";
              var placeholder = fieldInfos.placeholder ? fieldInfos.placeholder : "";

              this.content += input( modelField, inputType, placeholder, required );
            }
          }
        }
      }
    }
  }

  input() {
    return '\t\t<input name="' + modelField +
                '" type="' + inputType +
                '" placeholder="' + placeholder +
                '" value="<vue-escape> ' + modelField + ' </vue-escape>"'
                + required + '>\n';
  }

  vue() {


  }
}
